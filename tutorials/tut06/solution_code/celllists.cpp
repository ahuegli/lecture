#include "celllists.h"

#include <cassert>

CellListsView::CellListsView(const Domain& domain, real rc) :
    domain(domain),
    rc(rc)
{
    nCells.x = domain.localSize.x / rc;
    nCells.y = domain.localSize.y / rc;
    totNCells = nCells.x * nCells.y;

    h.x = domain.localSize.x / nCells.x;
    h.y = domain.localSize.y / nCells.y;
}

CellLists::CellLists(const Domain& domain, real rc) :
    CellListsView(domain, rc)
{
    starts.resize(totNCells + 1);
    counts.resize(totNCells);

    cellStarts = starts.data();
    cellCounts = counts.data();
}

void CellLists::build(std::vector<Particle>& particles)
{
    // compute counts

    for (auto& c : counts)
        c = 0;

    for (const auto& p : particles)
    {
        const int2 cid = positionToCidClamped(p.r);
        const int i = toLinearCid(cid);
        counts[i] ++;
    }

    // compute starts

    for (auto& s : starts)
        s = 0;

    for (size_t i = 0; i < counts.size(); ++i)
        starts[i+1] = starts[i] + counts[i];

    // find where each particle belongs and copy it to temporary helper

    helper.resize(particles.size());
    
    for (auto& c : counts)
        c = 0;

    for (const auto& p : particles)
    {
        const int2 cid = positionToCidClamped(p.r);
        const int i = toLinearCid(cid);

        const int subid = counts[i]++;
        const int dstId = starts[i] + subid;
        helper[dstId] = p;
    }

    // swap the particle vectors
    std::swap(particles, helper);
}

CellListsView CellLists::getView() const
{
    return static_cast<CellListsView>(*this);
}
