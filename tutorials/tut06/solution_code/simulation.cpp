#include "simulation.h"

#include "interactions.h"


Simulation::Simulation(MPI_Comm comm, int2 nRanks2D, const Domain& domain,
                       LennardJones interaction, real mass, real kBT, int n) :
    comm(comm),
    domain(domain),
    redistributor (comm, domain, nRanks2D),
    ghostExchanger(comm, domain, nRanks2D),
    cl(domain, interaction.rc),
    interaction(interaction),
    mass(mass)
{
    MPI_Check( MPI_Comm_rank(comm, &rank) );
    // particles = initializeRandomUniform(n, domain);
    particles = initializeGridUniform(n, domain, mass, kBT);
    forces.resize(particles.size());
}

void Simulation::advance(real dt)
{
    redistributor.redistribute(particles);
    cl.build(particles);

    forces.resize(particles.size());
    clearForces();
    
    ghostExchanger.exchangeGhosts(cl, particles, ghosts);
    computeBulk();
    computeHalo();

    velocityVerlet(dt);
    time += dt;
}

void Simulation::printStats(FILE *stream)
{
    constexpr int root = 0;
    const int nlocal = particles.size();
    int ntotal = 0;
    real vlocal[2] = {0.0_r, 0.0_r}, vtotal[2] = {0.0_r, 0.0_r};

    for (const auto p : particles)
    {
        vlocal[0] += p.v.x;
        vlocal[1] += p.v.y;
    }

    MPI_Check( MPI_Reduce(&nlocal, &ntotal, 1, MPI_INT,                 MPI_SUM, root, comm) );
    MPI_Check( MPI_Reduce( vlocal,  vtotal, 2, getMPIFloatType<real>(), MPI_SUM, root, comm) );

    if (rank == root)
    {
        fprintf(stream, "%g\t%d\t%g\t%g\n", time, ntotal, vtotal[0], vtotal[1]);
    }
}

void Simulation::dumpCsv(const std::string& filename) const
{
    dumpCSV(particles, comm, filename, domain);
}

void Simulation::dumpXyz(const std::string& filename) const
{
    dumpXYZ(particles, comm, filename, domain);
}

void Simulation::clearForces()
{
    for (auto& f : forces)
        f.f.x = f.f.y = 0.0_r;
}

void Simulation::computeBulk()
{
    computeSelfInteractions(interaction, particles, cl.getView(), forces);
}

void Simulation::computeHalo()
{
    computeExternalInteractions(interaction, ghosts, particles, cl.getView(), forces);
}

void Simulation::velocityVerlet(real dt)
{
    const auto invMass = 1.0_r / mass;
    
    for (size_t i = 0; i < particles.size(); ++i)
    {
        auto& p = particles[i];
        const auto f = forces[i].f;

        p.v.x += invMass * dt * f.x;
        p.v.y += invMass * dt * f.y;

        p.r.x += p.v.x * dt;
        p.r.y += p.v.y * dt;
    }
}
