#include "celllists.h"

#include <cassert>

CellListsView::CellListsView(const Domain& domain, real rc) :
    domain(domain),
    rc(rc)
{
    nCells.x = domain.localSize.x / rc;
    nCells.y = domain.localSize.y / rc;
    totNCells = nCells.x * nCells.y;

    h.x = domain.localSize.x / nCells.x;
    h.y = domain.localSize.y / nCells.y;
}

CellLists::CellLists(const Domain& domain, real rc) :
    CellListsView(domain, rc)
{
    starts.resize(totNCells + 1);
    counts.resize(totNCells);

    cellStarts = starts.data();
    cellCounts = counts.data();
}

void CellLists::build(std::vector<Particle>& particles)
{
    // compute counts

    // TODO

    // compute starts

    // TODO

    // reorder particles
    
    // TODO
}

CellListsView CellLists::getView() const
{
    return static_cast<CellListsView>(*this);
}
