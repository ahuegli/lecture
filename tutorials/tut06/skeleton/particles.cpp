#include "particles.h"
#include "utils.h"

#include <sstream>
#include <random>

std::vector<Particle> initializeRandomUniform(size_t n, const Domain& domain, long seed)
{
    std::vector<Particle> particles;
    particles.resize(n);
    const real2 L = domain.localSize;

    std::mt19937 gen(seed);
    std::uniform_real_distribution<real> distX(-0.5_r * L.x, 0.5_r * L.x);
    std::uniform_real_distribution<real> distY(-0.5_r * L.y, 0.5_r * L.y);

    for (auto& p : particles)
    {
        const real2 r {distX(gen), distY(gen)};
        const real2 v {0.0_r, 0.0_r};
        p = Particle{r, v};
    }
    return particles;
}

std::vector<Particle> initializeGridUniform(size_t n, const Domain& domain, real mass, real kBT, long seed)
{
    std::vector<Particle> particles;
    const real2 L = domain.localSize;

    std::mt19937 gen(seed);
    std::normal_distribution<real> dist(0.0_r, std::sqrt(kBT/mass/2.0_r));
    
    const real h = std::sqrt(L.x * L.y / n);
    const int nx = L.x / h;
    const int ny = L.y / h;

    const real dx = L.x / nx;
    const real dy = L.y / ny;

    real2 vtot {0.0_r, 0.0_r};

    particles.reserve(nx*ny);
    
    for (int iy = 0; iy < ny; ++iy)
    {
        for (int ix = 0; ix < nx; ++ix)
        {
            const real2 r {- 0.5_r * L.x + ix * dx,
                           - 0.5_r * L.y + iy * dy};
            const real2 v {dist(gen), dist(gen)};
            const Particle p{r, v};
            vtot.x += v.x;
            vtot.y += v.y;
            particles.push_back(p);
        }
    }

    vtot.x /= particles.size();
    vtot.y /= particles.size();
    
    for (auto& p : particles)
    {
        p.v.x -= vtot.x;
        p.v.y -= vtot.y;
    }
    
    return particles;
}

static inline std::string createCsvHeader()
{
    return "x,y,u,v\n";
}

static inline std::string createCsvContent(const std::vector<Particle>& particles, const Domain& domain)
{
    std::stringstream ss;

    for (const auto& p : particles)
    {
        const real2 r = domain.local2global(p.r);
        const real2 v = p.v;
        ss << r.x << "," << r.y << ","
           << v.x << "," << v.y << "\n";
    }

    return ss.str();
}

void dumpCSV(const std::vector<Particle>& particles, MPI_Comm comm, const std::string& filename, const Domain& domain)
{
    MPI_File fout;
    int rank;
    MPI_Check(MPI_Comm_rank(comm, &rank));

    MPI_File_open(comm, filename.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fout);

    if (rank == 0)
    {
        const std::string h = createCsvHeader();
        MPI_File_write_shared(fout, h.data(), h.size(), MPI_CHAR, MPI_STATUS_IGNORE);
    }

    const std::string csv = createCsvContent(particles, domain);
    MPI_File_write_ordered(fout, csv.data(), csv.size(), MPI_CHAR, MPI_STATUSES_IGNORE);
    MPI_File_close(&fout);
}

static inline std::string createXYZHeader(int ntot)
{
    std::stringstream ss;
    ss << ntot << "\n";
    ss << "LJ 2d cell lists\n";
    return ss.str();
}

static inline std::string createXYZContent(const std::vector<Particle>& particles, const Domain& domain)
{
    std::stringstream ss;

    for (const auto& p : particles)
    {
        const real2 r = domain.local2global(p.r);
        ss << "O " << r.x << " " << r.y << " 0.0\n";
    }

    return ss.str();
}

void dumpXYZ(const std::vector<Particle>& particles, MPI_Comm comm, const std::string& filename, const Domain& domain)
{
    constexpr int root = 0;
    
    MPI_File fout;
    int rank;
    MPI_Check(MPI_Comm_rank(comm, &rank));

    const int nlocal = particles.size();
    int ntotal = 0;

    MPI_Check(MPI_Reduce(&nlocal, &ntotal, 1, MPI_INT, MPI_SUM, root, comm));
    MPI_File_open(comm, filename.c_str(), MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fout);

    if (rank == root)
    {
        const std::string h = createXYZHeader(ntotal);
        MPI_File_write_shared(fout, h.data(), h.size(), MPI_CHAR, MPI_STATUS_IGNORE);
    }

    const std::string csv = createXYZContent(particles, domain);
    MPI_File_write_ordered(fout, csv.data(), csv.size(), MPI_CHAR, MPI_STATUSES_IGNORE);
    MPI_File_close(&fout);
}

