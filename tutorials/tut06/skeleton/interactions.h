#pragma once

#include "celllists.h"

#include <array>

struct LennardJones
{
    LennardJones(real rc, real epsilon, real sigma) :
        rc(rc),
        eps(epsilon),
        sigma2(sigma*sigma)
    {}

    Force apply(Particle src, Particle dst) const
    {
        const real2 dr {dst.r.x - src.r.x,
                        dst.r.y - src.r.y};

        const real dr2 = dr.x * dr.x + dr.y * dr.y;

        if (dr2 > rc*rc)
            return {0.0_r, 0.0_r};
        
        const real inv_dr2 = 1.0_r / dr2;

        const real sigma_r2  = sigma2 * inv_dr2;
        const real sigma_r6  = sigma_r2 * sigma_r2 * sigma_r2;
        const real sigma_r12 = sigma_r6 * sigma_r6;

        const real magnitude = 24.0_r * eps * inv_dr2 * (2.0_r * sigma_r12 - sigma_r6);

        const Force f {magnitude * dr.x,
                       magnitude * dr.y};
        return f;
    }

    real rc;
    real eps, sigma2;
};


template<typename Interaction>
void computeSelfInteractions(const Interaction interaction,
                             const std::vector<Particle>& particles,
                             const CellListsView& cellLists,
                             std::vector<Force>& forces)
{
    for (int dstId = 0; dstId < (int) particles.size(); ++dstId)
    {
        const auto dstP = particles[dstId];
        Force dstF {0.0_r, 0.0_r};
        const int2 dstCid = cellLists.positionToCidClamped(dstP.r);
        const int dstLCid = cellLists.toLinearCid(dstCid);

        int2 srcCid;
        
        for (srcCid.y  = std::max(0,                    dstCid.y-1);
             srcCid.y <= std::min(cellLists.nCells.y-1, dstCid.y+1);
             ++srcCid.y)
        {
            for (srcCid.x  = std::max(0,                    dstCid.x-1);
                 srcCid.x <= std::min(cellLists.nCells.x-1, dstCid.x+1);
                 ++srcCid.x)
            {
                const int srcLCid = cellLists.toLinearCid(srcCid);

                // skip half of the cells
                if (srcLCid > dstLCid) continue;

                const int start = cellLists.cellStarts[srcLCid];
                const int end   = cellLists.cellStarts[srcLCid + 1];
                
                for (int srcId = start; srcId < end; ++srcId)
                {
                    // skip half of the particles
                    if (srcId >= dstId) continue;
                    const Particle srcP = particles[srcId];
                    const Force f = interaction.apply(srcP, dstP);

                    dstF.f.x += f.f.x;
                    dstF.f.y += f.f.y;

                    forces[srcId].f.x -= f.f.x;
                    forces[srcId].f.y -= f.f.y;
                }
            }
        }
        forces[dstId].f.x += dstF.f.x;
        forces[dstId].f.y += dstF.f.y;
    }
}

template<typename Interaction>
void computeExternalInteractions(const Interaction interaction,
                                 const std::vector<Particle>& srcParticles,
                                 const std::vector<Particle>& dstParticles,
                                 const CellListsView& cellLists,
                                 std::vector<Force>& dstForces)
{
    // TODO
}
