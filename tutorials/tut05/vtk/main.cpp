#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stdio.h>
#include <mpi.h>

#define EV(x) (#x) << "=" << (x) << " "
#define EVV(x) std::cerr << (#x) << "=" << (x) << std::endl;

struct Point {
  double x, y, z;
};

using Poly = std::array<Point, 4>;
using PolyIdx = std::array<int, 4>;

std::vector<Poly> GetPoly(int rank, int commsize, int N) {
  std::vector<Point> vv;
  const double k = 0.15;
  const int n = (6.29 / k);
  for (int i = rank * N / commsize; i < (rank + 1) * N / commsize; ++i) {
    int j = i - (n + 1) * rank;
    double r = 1 + 0.5 * rank / commsize;
    double x = std::cos(j * k) * r;
    double y = std::sin(j * k) * r;
    double z = j * k * 0.04;
    vv.push_back({x, y, z});
  }
  std::vector<Poly> pp;
  for (size_t i = 0; i + n + 1 < vv.size(); ++i) {
    pp.push_back({vv[i], vv[i + 1], vv[i + n + 1], vv[i + n]});
  }
  return pp;
}

// Converts to index representation.
// vv: polygons as lists of points
// Returns:
// xx: points
// pp: polygons as lists of indices
void ConvertToIdx(const std::vector<Poly>& vv,
                  std::vector<Point>& xx, std::vector<PolyIdx>& pp) {
  xx.resize(0);
  pp.resize(0);
  for (auto& v : vv) {
    pp.emplace_back();
    for (size_t i = 0; i < v.size(); ++i) {
      pp.back()[i] = xx.size();
      xx.push_back(v[i]);
    }
  }
}

// Writes legacy vtk polydata
// xx: points
// pp: polygons as lists of indices
void WriteVtkPolyMpi(const std::string& path,
                     const std::vector<Point>& xx,
                     const std::vector<PolyIdx>& pp,
                     const std::string& comment, int rank, MPI_Comm comm) {
  MPI_File fout;
  MPI_File_open(comm, path.data(),
                MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fout);

  const int nx = xx.size(); // number of points
  int sum_nx = 0; // sum of nx
  MPI_Reduce(&nx, &sum_nx, 1, MPI_INT, MPI_SUM, 0, comm);

  if (rank == 0) {
    std::stringstream s;
    s <<  "# vtk DataFile Version 2.0\n"
        << comment + "\n"
        << "ASCII\n"
        << "DATASET POLYDATA\n"
        << "POINTS " << sum_nx << " float\n";
    std::string str = s.str();
    MPI_File_write_shared(fout, str.data(), str.size(),
                          MPI_CHAR, MPI_STATUS_IGNORE);
  }

  {
    std::stringstream s;
    for (auto& x : xx) {
      s << x.x << " " << x.y << " " << x.z << "\n";
    }
    std::string str = s.str();
    MPI_File_write_ordered(fout, str.data(), str.size(),
                           MPI_CHAR, MPI_STATUSES_IGNORE);
  }

  size_t np = pp.size(); // number of polygons
  size_t ni = 0; // number of indices
  for (auto& p : pp) {
    ni += p.size();
  }
  int sum_np = 0;
  int sum_ni = 0;
  MPI_Reduce(&np, &sum_np, 1, MPI_INT, MPI_SUM, 0, comm);
  MPI_Reduce(&ni, &sum_ni, 1, MPI_INT, MPI_SUM, 0, comm);

  if (rank == 0) {
    std::stringstream s;
    s << "POLYGONS  " << sum_np << " " << sum_np + sum_ni << "\n";
    std::string str = s.str();
    MPI_File_write_shared(fout, str.data(), str.size(),
                          MPI_CHAR, MPI_STATUS_IGNORE);
  }

  int offset = 0; // offset for index
  MPI_Exscan(&nx, &offset, 1, MPI_INT, MPI_SUM, comm);

  {
    std::stringstream s;
    for (auto& p : pp) {
      s << p.size();
      for (auto& k : p) {
        s << " " << k + offset;
      }
      s << "\n";
    }
    std::string str = s.str();
    MPI_File_write_ordered(fout, str.data(), str.size(),
                           MPI_CHAR, MPI_STATUSES_IGNORE);
  }
  MPI_File_close(&fout);
}


int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  MPI_Comm comm = MPI_COMM_WORLD;

  int size, rank;
  MPI_Comm_size(comm, &size);
  MPI_Comm_rank(comm, &rank);

  auto ppx = GetPoly(rank, size, 1000);

  std::vector<Point> xx;
  std::vector<PolyIdx> pp;
  ConvertToIdx(ppx, xx, pp);
  WriteVtkPolyMpi("poly.vtk", xx, pp, "comment", rank, comm);

  MPI_Finalize();
}
