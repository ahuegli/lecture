* To load modules on Euler:

    source modules.euler

  or

    . modules.euler

* Macro for Paraview to reload files

    _reloadall.py

  in GUI, use `Macros->Add new macro...`
