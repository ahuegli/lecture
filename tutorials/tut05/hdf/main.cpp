#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <stdio.h>
#include <mpi.h>
#include <hdf5.h>

#define EV(x) (#x) << "=" << (x) << " "
#define EVV(x) std::cerr << (#x) << "=" << (x) << std::endl;

struct Field {
  hsize_t nx, ny, nz;  // global size
  hsize_t x0, x1;      // local range in x
  std::vector<double> buf;
};

Field GetField(int nx, int ny, int nz, int rank, int commsize) {
  int x0 = rank * nx / commsize;
  int x1 = (rank + 1) * nx / commsize;
  Field u;
  u.nx = nx;
  u.ny = ny;
  u.nz = nz;
  u.x0 = x0;
  u.x1 = x1;
  u.buf.resize((u.x1 - u.x0) * u.ny * u.nz);
  int i = 0;
  for (int z = 0; z < nz; ++z) {
    for (int y = 0; y < ny; ++y) {
      for (int x = x0; x < x1; ++x) {
        int dx = x - nx / 2;
        int dy = y - ny / 2;
        int dz = z - nz / 2;
        u.buf[i] = std::sqrt(dx * dx + dy * dy + dz * dz);
        ++i;
      }
    }
  }
  return u;
}

void WriteXmf(std::string xmfpath, std::string hdfpath, std::string fieldname,
              int nx, int ny, int nz) {
  std::ofstream f(xmfpath);
  f << "<?xml version='1.0' ?>\n"
    << "<!DOCTYPE Xdmf SYSTEM 'Xdmf.dtd' []>\n"
    << "<Xdmf Version='2.0'>\n"
    << " <Domain>\n"
    << "   <Grid GridType='Uniform'>\n"
    << "     <Topology TopologyType='3DCORECTMesh' Dimensions='"
    // 'nx+1' number of nodes
    << nz + 1 << " " << ny + 1 << " " << nx + 1 << "'/>\n\n"
    << "     <Geometry GeometryType='ORIGIN_DXDYDZ'>\n"
    << "       <DataItem Name='Origin' Dimensions='3' NumberType='Float' Precision='8' Format='XML'>\n"
    << "         0 0 0\n"
    << "       </DataItem>\n"
    << "       <DataItem Name='Spacing' Dimensions='3' NumberType='Float' Precision='8' Format='XML'>\n"
    << "         1 1 1\n"
    << "       </DataItem>\n"
    << "     </Geometry>\n\n"
    << "     <Attribute Name='" << fieldname
    << "' AttributeType='Scalar' Center='Cell'>\n"
    << "       <DataItem Dimensions='"
    // 'nx' number of cells
    << nz << " " << ny << " " << nx
    << "' NumberType='Float' Precision='8' Format='HDF'>\n"
    << "        " << hdfpath << ":/data\n"
    << "       </DataItem>\n"
    << "     </Attribute>\n"
    << "   </Grid>\n"
    << " </Domain>\n"
    << "</Xdmf>\n";
}

void WriteHdf(std::string hdfpath, const Field& u, MPI_Comm comm) {
  constexpr int DIM = 3;
  hid_t file_id, dataset_id, fspace_id, fapl_id, mspace_id;

  fapl_id = H5Pcreate(H5P_FILE_ACCESS);
  H5Pset_fapl_mpio(fapl_id, comm, MPI_INFO_NULL);
  // Create file
  file_id = H5Fcreate(hdfpath.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);
  H5Pclose(fapl_id);

  hsize_t count[DIM] = {u.nz, u.ny, u.x1 - u.x0}; // local size
  hsize_t dims[DIM] = {u.nz, u.ny, u.nx};         // global size
  hsize_t offset[DIM] = {0, 0, u.x0};

  fapl_id = H5Pcreate(H5P_DATASET_XFER);
  H5Pset_dxpl_mpio(fapl_id, H5FD_MPIO_COLLECTIVE); // enable MPI IO
  // Create file space
  fspace_id = H5Screate_simple(DIM, dims, NULL);
  // Create dataset
  dataset_id = H5Dcreate(file_id, "data", H5T_NATIVE_DOUBLE, fspace_id,
                         H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

  // Select slab in filespace
  H5Sselect_hyperslab(fspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);
  // Create memory space
  mspace_id = H5Screate_simple(DIM, count, NULL);
  // Write to file
  H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, mspace_id,
           fspace_id, fapl_id, u.buf.data());

  H5Sclose(fspace_id);
  H5Sclose(mspace_id);
  H5Dclose(dataset_id);
  H5Pclose(fapl_id);
  H5Fclose(file_id);
}

int main(int argc, char *argv[]) {
  MPI_Init(&argc, &argv);
  MPI_Comm comm = MPI_COMM_WORLD;

  int size, rank;
  MPI_Comm_size(comm, &size);
  MPI_Comm_rank(comm, &rank);

  auto u = GetField(8, 16, 24, rank, size);
  std::string hdfpath = "grid.h5";
  WriteHdf("grid.h5", u, comm);
  WriteXmf("grid.xmf", hdfpath, "u", u.nx, u.ny, u.nz);

  MPI_Finalize();
}
