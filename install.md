# Add the fork remote
git remote add upstream git@gitlab.ethz.ch:hpcse19/lecture.git

# update from upstream
git fetch upstream && git pull upstream public
