#include <iostream>
#include <omp.h>
#include <vector>


int main(int argc, char const *argv[]) {

    std::vector<double> a(50,50);
    std::vector<double> b(50,50);
    std::vector<double> c(50,50);

    int n = 50;

    #pragma omp parallel for nowait
    for(int i=1; i < n; i++)
    {
        b[i-1] = (a[i]+a[i-1])/2;
        c[i-1] += a[i];
    }

    for(int i = 0; i < a.size(); i++)
        std::cout << a[i] << "\n" << b[i] << "\n" << c[i] << "\n";

    return 0;
}
