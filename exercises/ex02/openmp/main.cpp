#include <stdio.h>
#include <stdlib.h>
#include <random>
#include <fstream>
#include <iostream>
#include <cmath>


void do_work(const float a, const float sum);
double new_value(int i);

void time_loop() {
    float t = 0;
    float sum = 0;

    #pragma omp parallel
    {

        for(int step=0; step < 100; step++)
        {
            #pragma omp parallel for nowait
            for(int i=1; i < n; i++)
            {
                b[i-1] = (a[i]+a[a-1])/2;
                c[i-1] += a[i];
            }

            #pragma omp for
            for (int i=0;i < m; i++)
                z[i] = std::sqrt(b[i]+c[i]);
            
            #pragma omp for reduction(+:sum)
            for(int i=0; i<m; i++)
                sum = sum + z[i];
            
            #pragma omp critical
            {
                do_work(t, sum);
            }

            #pragma omp single
            {
                t = new_value(step);
            }
        }
    }
}