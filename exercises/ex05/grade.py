#!/usr/bin/env python
# File       : grade.py
# Description: Generate grading submission file
# Copyright 2018 ETH Zurich. All Rights Reserved.
question_conf = {
        'Name' : 'Exercise 4',
        'steps' : ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'],
        'Questions' : {
            'Question a': {'Total Points': 3},
            'Question b': {'Total Points': 3},
            'Question c': {'Total Points': 3},
            'Question d': {'Total Points': 3},
            'Question e': {'Total Points': 3},
            'Question f': {'Total Points': 3},
            'Question g': {'Total Points': 5},
            'Question h': {'Total Points': 2},
            'Question i': {'Total Points': 3},
            'Question j': {'Total Points': 3},
            'Question k': {'Total Points': 3},
            'Question l': {'Total Points': 3}
            }
        }

import argparse
import datetime
import sys

def parse_args():
    parser = argparse.ArgumentParser()

    for q in question_conf['steps']:
        parser.add_argument('-q%s' % q, '--question%s' % q,
                type=float, default=0,
                help='Scored points for Question %s' % q)
        parser.add_argument('-c%s' % q,'--comment%s' % q,
                type=str, action='append', nargs='*',
                help='Comments for Question %s (you can add multiple comments)' % q)

    return vars(parser.parse_args())

if __name__ == "__main__":
    args = parse_args()

    grade = lambda s,m: min(3.0 + (6.0-3.0) * float(s)/m, 6.0)
    summary = {}
    score = 0
    maxpoints = 31.0
    header = '{name:s}: {date:s}\n'.format(
        name = question_conf['Name'], date = str(datetime.datetime.now()))
    width = len(header.rstrip())
    summary[0] = [header]
    i = 0
    for q in question_conf['steps']:
        content = []
        qscore  = args['question%s' % q]
        qmax    = question_conf['Questions']['Question %s' % q]['Total Points']
        qscore  = max(0 , min(qscore, qmax))
        content.append( 'Question %s: %f/%f\n' % (q, qscore, qmax) )
        comments = args['comment%s' % q]
        if comments is not None:
            for j,comment in enumerate([s for x in comments for s in x]):
                content.append( ' -Comment {id:d}: {issue:s}\n'.format(
                    id = j+1, issue = comment.strip())
                    )
        for line in content:
            width = width if len(line.rstrip())<width else len(line.rstrip())
        score += qscore
        summary[i] = content
        i = i + 1

    with open('grade.txt', 'w') as out:
        out.write(width*'*'+'\n')
        for lines in summary.values():
            for line in lines:
                out.write(line)
            out.write(width*'*'+'\n')
        out.write('Grade: {:.2f}'.format(grade(score, maxpoints)))
