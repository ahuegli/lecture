#!/bin/bash

for i in 4 
do 
	export OMP_NUM_THREADS=$i
	bsub -R "span[ptile=$i]" -n $i -o $i ./exec_serial
	echo sumbitted with $OMP_NUM_THREADS
done
