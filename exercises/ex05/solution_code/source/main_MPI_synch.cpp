#include <fstream>

#include "Profiler.h"

#include "SerialParticlesIterator.h"
using namespace SerialParticlesIterator;

#include <mpi.h>

int main (int argc, char ** argv)
{
    Profiler profiler;
    MPI_Init(& argc, & argv);
    int mpi_rank=0, mpi_size=1;
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

    size_t global_n_particles = 5040;

    if (argc > 1) global_n_particles = std::stoul(argv[1]);
    if (mpi_rank==0)
    {
        printf("Simulating %lu particles with %d MPI processes.\n",
                            global_n_particles, mpi_size);
        if (argc < 2) printf("To change N particles run as '%s N'\n", argv[0]);
    }
    
    // number of particles
    // each rank gets a fraction of the global number of particles,
    // distributed along a fracion of the global simulation extent [-0.5, 0.5]
    const size_t n_particles = global_n_particles / mpi_size;
    const value_t extent_x   = 1.0 / mpi_size;
    const value_t vol_p = 1.0 / global_n_particles;
    const value_t start_x = - 0.5 + mpi_rank * extent_x;
    const value_t end_x = start_x + extent_x;

    // time integration setup:
    const value_t end_time = 2.5;
    const value_t print_freq = 0.1;
    value_t print_time = 0;
    const value_t dt = 0.0001;

    // initialize particles: position and circulation
    std::function<value_t(value_t)> gamma_init_1D = [&] (const value_t x) {
        return vol_p * 4 * x / std::sqrt(1 - 4 * x * x);
    };
    ArrayOfParticles particles = initialize_particles_1D(
        n_particles, start_x, end_x, gamma_init_1D);

    value_t time = 0;
    size_t step = 0;
    while (time < end_time)
    {
        // 0. init velocities to zero
        profiler.start("clear");
        reset_velocities(particles);
        profiler.stop("clear");

        // 2. compute local
        // instantiate array where to receive other ranks' arrays:
        ArrayOfParticles exchange(particles.size());

        profiler.start("velocity");
        for (int pass=0; pass < mpi_size; ++pass)
        {
            // 1. exchange
            profiler.start("comm");
            if (pass > 0) // at pass zero, compute_interaction with own particles
            {
                MPI_Request reqs[6];
                // remember: no need to send velocities
                const int rank_to   = (mpi_rank + pass) % mpi_size;
                const int rank_from = (mpi_rank - pass  + mpi_size) % mpi_size;
                MPI_Isend(particles.pos_x(), particles.size(), MPI_VALUE_T,
                          rank_to,   0, MPI_COMM_WORLD, &reqs[0]);
                MPI_Isend(particles.pos_y(), particles.size(), MPI_VALUE_T,
                          rank_to,   1, MPI_COMM_WORLD, &reqs[2]);
                MPI_Isend(particles.gamma(), particles.size(), MPI_VALUE_T,
                          rank_to,   2, MPI_COMM_WORLD, &reqs[4]);

                MPI_Irecv( exchange.pos_x(), particles.size(), MPI_VALUE_T,
                          rank_from, 0, MPI_COMM_WORLD, &reqs[1]);
                MPI_Irecv( exchange.pos_y(), particles.size(), MPI_VALUE_T,
                          rank_from, 1, MPI_COMM_WORLD, &reqs[3]);
                MPI_Irecv( exchange.gamma(), particles.size(), MPI_VALUE_T,
                          rank_from, 2, MPI_COMM_WORLD, &reqs[5]);

                MPI_Waitall(6, reqs, MPI_STATUSES_IGNORE);
            }
            profiler.stop("comm");

            // 2. compute local
            profiler.start("compute");
            ArrayOfParticles & sources = pass == 0? particles : exchange;
            compute_interaction(sources, particles);
            profiler.stop("compute");
        }
        profiler.stop("velocity");

        // 3. with new velocities, advect particles positions:
        profiler.start("advect");
        advect_euler(particles, dt);
        profiler.stop("advect");

        // 4. debug measure: make sure circulation stays constant
        profiler.start("sum gamma");
        value_t total_circulation = sum_circulation(particles);
        MPI_Allreduce(MPI_IN_PLACE, &total_circulation, 1, MPI_VALUE_T,
                      MPI_SUM, MPI_COMM_WORLD);
        profiler.stop("sum gamma");

        if ((time+dt) > print_time)
        {
            print_time += print_freq;

            {
                // print to file
                profiler.start("fwrite");
                std::string config = particles.convert2csv();
                char fname[128]; sprintf(fname, "mpi_config_%06lu.csv", step+1);

                MPI_File fout;
                if ( MPI_File_open(MPI_COMM_WORLD, fname, 
                       MPI_MODE_CREATE | MPI_MODE_WRONLY | MPI_MODE_UNIQUE_OPEN,
                       MPI_INFO_NULL, &fout) )
                {
                    printf("FATAL: Can not create MPI file %s\n", fname);
                    exit(1);
                }

                MPI_File_write_ordered(fout, config.c_str(), config.size(),
                    MPI_CHAR, MPI_STATUS_IGNORE);
                MPI_File_close(&fout);
                profiler.stop("fwrite");
            }

            if (mpi_rank == 0)
            {
                if (time > 0) profiler.printStatAndReset();
                printf("Iteration %lu - time %f - Total Circulation %f\n",
                       step, time, total_circulation);
            }
        }

        // advance time integration:
        step++;
        time += dt;
    }

    MPI_Finalize();
	return 0;
}
