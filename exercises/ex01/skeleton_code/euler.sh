# Your email address for euler notification
# Leave empty for no notification
EMAIL=ahuegli@student.ethz.ch

WORKING_DIR=HPCSE_I/ex01 # Name of the folder on euler
EXECUTABLE_FILE=benchmark_1Ddiffusion # File to be executed on euler
OUTPUT_FILE=output.txt # Name of the output file (saved in $SCRATCH)

MODULES=(gcc/4.8.2) # Euler modules (space separated)

echo "Update the code"
rsync -ah --progress --delete . euler.ethz.ch:$WORKING_DIR
wait

echo "Connecting to euler.ethz.ch..."
ssh euler.ethz.ch /bin/bash <<EOSSH
    echo -e "\e[32mSet the nesseary variables\e[0m" # -e for color...
    EMAIL=$EMAIL                                    # $  uses the local va
    WORKING_DIR=\${HOME}/$WORKING_DIR               # \$ uses the remote var
    EXECUTABLE=\${WORKING_DIR}/$EXECUTABLE_FILE
    OUTPUT=\${SCRATCH}/$OUTPUT_FILE

    echo -e "\e[32mThe variables are:\e[0m"
    echo "EMAIL=\${EMAIL}"
    echo "HOME=\${HOME}"
    echo "SCRATCH=\${SCRATCH}"
    echo "WORKING_DIR=\${WORKING_DIR}"
    echo "EXECUTABLE=\${EXECUTABLE}"
    echo "OUTPUT=\${OUTPUT}"

    echo -e "\e[32mChange to the directory\e[0m"
    cd \${WORKING_DIR}

    echo -e "\e[32mLoad the modules\e[0m"
    echo ${MODULES[*]}
    module load ${MODULES[*]}

    echo -e "\e[32mCompile the code\e[0m"
    make

    echo -e "\e[32mAdd the job to the batch system\e[0m"
    bsub -o batch.log -W 4:00 -n 24 <<EOBSUB
        # Send email if EMAIL is set
        # Uncomment if you need an email on start:
        # [ -z \${EMAIL} ] || bbjobs | mail -s 'Job started' \${EMAIL}

        # Execute the script
        \${EXECUTABLE} > \${OUTPUT}

        # Send email if EMAIL is set (-a append the result)
        [ -z \${EMAIL} ] || bbjobs | mail -s 'Job ended' -a \${OUTPUT} \${EMAIL}
# Cannot have an indentation, otherwise it wont be recognized
EOBSUB
    [ -z \${EMAIL} ] || echo -e "\e[32mYou will be notified when the job finishes.\e[0m"
    bjobs
EOSSH
