// File       : benchmark_1Ddiffusion.cpp
// Description: Benchmark 1D diffusion test
// Copyright 2019 ETH Zurich. All Rights Reserved.
#include <iostream>
#include <vector>
#include <chrono> // use std::chrono to benchmark your code
#include <cmath>

class CacheFlusher
{
public:
    using Size = long long int;
    CacheFlusher(Size size = 1<<26 /* 64 MB */) :
        size(size),
        buf(new volatile unsigned char[size])
    {}

    ~CacheFlusher()
    {
        delete[] buf;
    }

    void flush()
    {
        for (Size i = 0; i < size; ++i)
            buf[i] += i;
    }
    
private:
    const Size size {0};
    volatile unsigned char *buf;
};

int main()
{
    // convenient tool to flush the cache
    // use it as cacheFlusher.flush()
    CacheFlusher cacheFlusher;

    //constants
    size_t N = 1<<21;
    double L = 1000;
    double alpha = 1.0e-4;
    double dx = L/(N-1);
    double dt = (dx*dx)/(2*alpha);
    double dtalphadx = (dt*alpha)/(dx*dx);

    //initialize vectors
    std::vector<double> u(N);
    std::vector<double> u1(N);
    std::vector<double> x(N);

    for(size_t i = 0; i < N; ++i)
        x[i] = i*dx;
    
    for(size_t i = 1; i < N; i++) {
        u[i] = std::sin(2.0*M_PI/L*x[i]);
    }


    std::chrono::duration<double> t_total(0);
    for(size_t i = 0; i < 1000; i++){
        const auto t0 = std::chrono::high_resolution_clock::now();
        //calculate u_n+1
        u1[0] = u[0] + dtalphadx*(u[N-1] - 2.0*u[0] + u[1]);
        for(size_t i = 1; i < N-1; ++i) {
            u1[i] = u[i] + dtalphadx*(u[i-1] - 2.0*u[i] + u[i+1]);
        }
        u1[N-1] = u[N-1] + dtalphadx*(u[N-2] - 2.0*u[N-1] + u[0]);
        const auto t1 = std::chrono::high_resolution_clock::now();
        t_total += (t1-t0);
<<<<<<< HEAD

=======
>>>>>>> 384ab65a8fb95d039fda851b1e700b3af3915b0b
        cacheFlusher.flush();
    }
    
    std::cout << t_total.count() << "\n";


    
    return 0;
}
