#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <omp.h>
#include <string>
#include <vector>

// sample collection for diagnostics
struct Diagnostics {
    double time;
    double heat;
    Diagnostics(double time, double heat) : time(time), heat(heat) {}
};

class Diffusion2D
{
public:
    Diffusion2D(const double D, const double L, const int N, const double dt)
        : D_(D), L_(L), N_(N), dt_(dt)
    {
        // Real space grid spacing.
        dr_ = L_ / (N_ - 1);

        // Actual dimension of a row (+2 for the ghost cells).
        real_N_ = N + 2;

        // Total number of cells.
        Ntot_ = (N_ + 2) * (N_ + 2);

        phi_.resize(Ntot_, 0.);
        rhs_.resize(Ntot_, 0.);

        // Initialize field on grid
        initialize_phi();

        // Common pre-factor
        R_ = D * dt / (2. * dr_ * dr_);

        // Initialize diagonals of the coefficient
        // matrix A, where Ax=b is the corresponding
        // system to be solved
        a_.resize(real_N_-2, -R_);
        b_.resize(real_N_-2, 1+2*R_);
        c_.resize(real_N_-2, -R_);
        // a_[N-1] = 0.0;
        // b_[0] = 1.0;
        // b_[N] = 1.0;
        // c_[0] = 0.0;
        // for (int i = 1; i < real_N_; ++i){
        //     b_[i] -= c_[i-1]*(a_[i-1]/b_[i-1]);
        // }

    }


    void advance()
    {
        // TODO:
        // Implement the ADI scheme for diffusion
        // and parallelize with OpenMP

        // ADI Step 1: Update rows at half timestep
        // Solve implicit system with Thomas algorithm
        #pragma omp parallel for
        for(int i = 1; i < real_N_; ++i){
            for(int j = 1; j < real_N_; ++j){
                int d = i * real_N_ + j;
                int ld = (i-1) * real_N_ + j;
                int ud = (i+1) * real_N_ + j;

                rhs_[d] = phi_[d] + R_ * (phi_[ld] - 2*phi_[d] + phi_[ud]);
            }
        }

        #pragma omp parallel for
        for(int i = 1; i < real_N_ - 1; ++i){
            thomas(0, i);
        }

        // ADI: Step 2: Update columns at full timestep
        // Solve implicit system with Thomas algorithm
        #pragma omp parallel for
        for(int i = 1; i < real_N_; ++i){
            for(int j = 1; j < real_N_; ++j){
                int d = i * real_N_ + j;
                int ld = i * real_N_ + (j-1);
                int ud = i * real_N_ + (j+1);

                rhs_[d] = phi_[d] + R_ * (phi_[ld] - 2*phi_[d] + phi_[ud]);
            }
        }

        #pragma omp parallel for
        for(int j = 1; j < real_N_ - 1; ++j){
            thomas(1,j);
        }

    }

    void compute_diagnostics(const double t)
    {
        double heat = 0.0;

        // TODO:
        // Compute the integral of phi_ in the computational domain
        #pragma omp parallel for reduction(+:heat)
        for(int i = 1; i < real_N_; ++i){
            for(int j = 1; j < real_N_; ++j){
                heat += dr_ * dr_ * phi_[i * real_N_ + j];
            }
        }

#ifndef NDEBUG
        std::cout << "t = " << t << " heat = " << heat << '\n';
#endif
        diag_.push_back(Diagnostics(t, heat));
    }

    void write_diagnostics(const std::string &filename) const
    {
        std::ofstream out_file(filename, std::ios::out);
        for (const Diagnostics &d : diag_)
            out_file << d.time << '\t' << d.heat << '\n';
        out_file.close();
    }

private:

    int dirtoGlobal(const bool dir, const int a, const int b){
        if(dir == 0)
            return a * real_N_ + b;
        else
            return b * real_N_ + a;
    }

    void thomas(bool dir, const int n){
        std::vector<double> v(N_);
        std::vector<double> bp(N_);
        std::vector<double> vp(N_);

        int i, k, k1;

        v[0] = dir == 0 ? rhs_[n*real_N_] : rhs_[n];
        bp[0] = b_[0];
        vp[0] = v[0];

        for(int i = 1; i < real_N_-1; i++){
            k = dirtoGlobal(dir, n, i+1);
            v[i] = rhs_[k];
            bp[i] = b_[i] - c_[i-1] * a_[i-1] / bp[i-1];
            vp[i] = v[i] - vp[i-1] * a_[i] / bp[i-1];
            //vp[i] = v[i] - vp[i-1] * a_[i] / b_[i-1];
        }

        i = N_ - 1;
        k = dirtoGlobal(dir, n, i+1);
        vp[i] = v[i] - vp[i-1] * a_[i] / bp[i-1];
        //vp[i] = v[i] - vp[i-1] * a_[i] / b_[i-1];

        k = dirtoGlobal(dir, n, real_N_ - 2);
        phi_[k] = vp[real_N_-1] / bp[real_N_-1];
        //phi_[k] = vp[real_N_-1] / b_[real_N_-1];
        for(int j = real_N_-2; j >= 0; --j){
            k = dirtoGlobal(dir, n, j+1);
            k1 = dirtoGlobal(dir, n, j+2);
            phi_[k] = (vp[j] - c_[j]*phi_[k1]) / bp[j];
            //phi_[k] = (vp[j] - c_[j]*phi_[k1]) / b_[j];
        }
    }

    void initialize_phi()
    {
        // Initialize phi(x, y, t=0)
        double bound = 0.25 * L_;

        // Initialize field phi based on the
        // prescribed initial conditions
        // and parallelize with OpenMP
        #pragma omp parallel for
        for (int i = 1; i < real_N_ - 1; ++i)     // rows
            for (int j = 1; j < real_N_ - 1; ++j) // columns
            {
                int idx = i * real_N_ + j;
                if(std::abs((i-1)*dr_ - L_/2.) < bound && std::abs((j-1)*dr_ - L_/2.) < bound)
                    phi_[idx] = 1.0;
                else
                    phi_[idx] = 0.0;
            }
    }

    double D_, L_;
    int N_, Ntot_, real_N_;
    double dr_, dt_;
    double R_;
    std::vector<double> phi_, rhs_;
    std::vector<Diagnostics> diag_;
    std::vector<double> a_, b_, c_;
};

// No additional code required from this point on
int main(int argc, char *argv[])
{
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " D L N dt\n";
        return 1;
    }

#pragma omp parallel
#pragma omp master
        std::cout << "Running with " << omp_get_num_threads() << " threads\n";

    const double D = std::stod(argv[1]);  // diffusion constant
    const double L = std::stod(argv[2]);  // domain side (length)
    const int N = std::stoul(argv[3]);    // number of grid points per dimension
    const double dt = std::stod(argv[4]); // timestep
    const std::string filename = argv[5];

    Diffusion2D system(D, L, N, dt);

    const auto start = std::chrono::system_clock::now();
    for (int step = 0; step < 10000; ++step) {
        system.advance();
        system.compute_diagnostics(dt * step);
    }
    const auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    std::cout << "Timing: "
              << "N=" << N << " elapsed=" << elapsed.count() << " ms" << '\n';

    system.write_diagnostics(filename);

    return 0;
}
