#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2018 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 10 \ # my scored points
#     --comment1 'Add a comment to justify your score' \ # optional
#     --comment1 'Add a comment if you had problems somewhere' \ # optional
#     --question2 50 \ # my scored points
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle:
# https://moodle-app2.let.ethz.ch/course/view.php?id=11566

python grade.py \
    --question1 43 \
    --comment1 "5 points reduction for not including boundary conditions" \
    --comment1 "did not explain what is observed" \
    --question2 45 \
    --comment2 "5 points reduction for neglecting periodicity"
