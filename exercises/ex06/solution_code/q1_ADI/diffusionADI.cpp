#include <algorithm>
#include <cassert>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <omp.h>
#include <string>
#include <vector>

// sample collection for diagnostics
struct Diagnostics {
    double time;
    double heat;
    Diagnostics(double time, double heat) : time(time), heat(heat) {}
};

class Diffusion2D
{
public:
    Diffusion2D(const double D, const double L, const int N, const double dt)
        : D_(D), L_(L), N_(N), dt_(dt)
    {
        // Real space grid spacing.
        dr_ = L_ / (N_ - 1);

        // Actual dimension of a row (+2 for the ghost cells).
        real_N_ = N + 2;

        // Total number of cells.
        Ntot_ = (N_ + 2) * (N_ + 2);

        phi_.resize(Ntot_, 0.);
        rhs_.resize(Ntot_, 0.);

        // Initialize field on grid
        initialize_phi();

        // Common pre-factor
        R_ = D * dt / (2. * dr_ * dr_);

        // Initialize diagonals of the coefficient
        // matrix A, where Ax=b is the corresponding
        // system to be solved (see also slides_ex06.pdf)
        b_ = -R_;
        a_ = 1. + 2. * R_;
        c_ = -R_;
    }

    void advance()
    {
        // ADI Step 1: Solve a tridiagonal system for each row
#pragma omp parallel for
        for (int iy = 1; iy < real_N_ - 1; iy++)     // rows
            for (int ix = 1; ix < real_N_ - 1; ix++) // columns
            {
                int k = iy * real_N_ + ix;
                int k1 = (iy - 1) * real_N_ + ix;
                int k2 = (iy + 1) * real_N_ + ix;
                rhs_[k] = phi_[k] + R_ * (phi_[k1] - 2. * phi_[k] + phi_[k2]);
            }
#pragma omp parallel for
        for (int iy = 1; iy < real_N_ - 1; iy++) // rows
            thomas(0, iy);

            // ADI Step 2: Solve a tridiagonal system for each column
#pragma omp parallel for
        for (int iy = 1; iy < real_N_ - 1; iy++)     // rows
            for (int ix = 1; ix < real_N_ - 1; ix++) // columns
            {
                int k = iy * real_N_ + ix;
                int k1 = iy * real_N_ + (ix - 1);
                int k2 = iy * real_N_ + (ix + 1);
                rhs_[k] = phi_[k] + R_ * (phi_[k1] - 2. * phi_[k] + phi_[k2]);
            }
#pragma omp parallel for
        for (int ix = 1; ix < real_N_ - 1; ix++) // columns
            thomas(1, ix);
    }

    void compute_diagnostics(const double t)
    {
        double heat = 0.0;
#pragma omp parallel for reduction(+ : heat)
        for (int i = 1; i < real_N_ - 1; ++i)
            for (int j = 1; j < real_N_ - 1; ++j)
                heat += dr_ * dr_ * phi_[i * real_N_ + j];

#ifndef NDEBUG
        std::cout << "t = " << t << " heat = " << heat << '\n';
#endif
        diag_.push_back(Diagnostics(t, heat));
    }

    void write_diagnostics(const std::string &filename) const
    {
        std::ofstream out_file(filename, std::ios::out);
        for (const Diagnostics &d : diag_)
            out_file << d.time << '\t' << d.heat << '\n';
        out_file.close();
    }

    void write(const int step, const double t)
    {
        std::ofstream out("dump" + std::to_string(step) + ".dat");
        out << "# time: " << t << '\n';
        out << "# step: " << step << '\n';
        out << "# dim: " << N_ << '\n';
        for (int i = 1; i < real_N_ - 1; ++i)     // rows
            for (int j = 1; j < real_N_ - 1; ++j) // columns
            {
                int k = i * real_N_ + j;
                out << phi_[k] << '\n';
            }
    }

private:

    /// @brief Thomas algorithm applied for row/col 'off' in direction dir
    ///
    /// @param dir Direction 0=x (off corresponds to rows) or 1=y (off
    ///            corresponds to columns)
    /// @param off Offset for system to be solved (row or column offset)
    void thomas(const int dir, const int off)
    {
        std::vector<double> L_(N_);
        std::vector<double> U_(N_);
        std::vector<double> z_(N_);

        int k;
        
        // LU decomposition and forward substitution
        {
            // Note the + 1 because of the integrated boundary conditions in the
            // array we use k to index.
            k = (0 == dir) ? (off * real_N_ + 1) : (real_N_ + off);
            L_[0] = a_;
            U_[0] = b_ / a_;
            z_[0] = rhs_[k] / L_[0];
            for (int i = 1; i < N_ - 1; ++i) {
                k = (0 == dir) ? (off * real_N_ + (i + 1))
                               : ((i + 1) * real_N_ + off);
                L_[i] = a_ - c_ * U_[i - 1];
                const double Linv = 1.0 / L_[i];
                U_[i] = b_ * Linv;
                z_[i] = (rhs_[k] - c_ * z_[i - 1]) * Linv;
            }
        }
        k = (0 == dir) ? (off * real_N_ + N_)
                       : ((N_)*real_N_ + off);
        L_[N_ - 1] = a_ - c_ * U_[N_ - 2];
        phi_[k] = (rhs_[k] - c_ * z_[N_ - 2]) / L_[N_ - 1];

        // backward substitution
        int kp1 = k;
        for (int i = N_ - 2; i >= 0; --i) {
            k = (0 == dir) ? (off * real_N_ + (i + 1))
                           : ((i + 1) * real_N_ + off);
            phi_[k] = z_[i] - U_[i] * phi_[kp1];
            kp1 = k;
        }
    }

    void initialize_phi()
    {
        // Initialize phi(x, y, t=0)
        double bound = 0.25 * L_;

#pragma omp parallel for
        for (int i = 1; i < real_N_ - 1; ++i)     // rows
            for (int j = 1; j < real_N_ - 1; ++j) // columns
            {
                int k = i * real_N_ + j;
                if (std::abs((i - 1) * dr_ - L_ / 2.) < bound &&
                    std::abs((j - 1) * dr_ - L_ / 2.) < bound)
                    phi_[k] = 1.;
                else
                    phi_[k] = 0.;
            }
    }

double D_,
    L_;
    int N_, Ntot_, real_N_;
    double dr_, dt_;
    double R_;
    std::vector<double> phi_, rhs_;
    std::vector<Diagnostics> diag_;
    double a_, b_, c_; // diagonal and off-diagonals
};

// No additional code required from this point on
int main(int argc, char *argv[])
{
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " D L N dt\n";
        return 1;
    }

#pragma omp parallel
#pragma omp master
        std::cout << "Running with " << omp_get_num_threads() << " threads\n";

    const double D = std::stod(argv[1]);  // diffusion constant
    const double L = std::stod(argv[2]);  // domain side (length)
    const int N = std::stoul(argv[3]);    // number of grid points per dimension
    const double dt = std::stod(argv[4]); // timestep

    Diffusion2D system(D, L, N, dt);

    const auto start = std::chrono::system_clock::now();
    for (int step = 0; step < 10000; ++step) {
        system.advance();
        system.compute_diagnostics(dt * step);
    }
    const auto end = std::chrono::system_clock::now();
    auto elapsed =
        std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    std::cout << "Timing: "
              << "N=" << N << " elapsed=" << elapsed.count() << " ms" << '\n';

    system.write_diagnostics("diagnostics.dat");

    return 0;
}
