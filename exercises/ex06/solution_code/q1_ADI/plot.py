#!/usr/bin/env python
# File       : plot.py
# Created    : Thu Dec 19 2019 11:24:24 AM (+0100)
# Author     : Fabian Wermelinger
# Description: Plot 2D contour
# Copyright 2019 ETH Zurich. All Rights Reserved.
import numpy as np
import matplotlib.pyplot as plt
import argparse

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', type=str, help='Input file')
    return parser.parse_known_args()

def main():
    args, _ = parseArgs()
    with open(args.file, 'r') as header:
        t = float(header.readline().strip().split()[2])
        s = int(header.readline().strip().split()[2])
        N = int(header.readline().strip().split()[2])
    f = np.loadtxt(args.file).reshape((N,N))
    plt.contourf(f, np.linspace(0, 1, 20))
    plt.gca().get_xaxis().set_visible(False)
    plt.gca().get_yaxis().set_visible(False)
    plt.axis('off')
    plt.axis('equal')
    plt.title("Time: {:.3e}".format(t))
    plt.savefig("dump{:d}.png".format(s), dpi=200, bbox_inches='tight')

if __name__ == "__main__":
    main()
