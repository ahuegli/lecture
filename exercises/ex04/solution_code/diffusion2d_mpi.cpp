#include <iostream>
#include <algorithm>
#include <string>
#include <fstream>
#include <cassert>
#include <vector>
#include <cmath>
#include <mpi.h>
#include <stdio.h>
#include "timer.hpp"


struct Diagnostics
{
    double time;
    double heat;

    Diagnostics(double time, double heat) : time(time), heat(heat) {}
};


class Diffusion2D_MPI
{
public:
    Diffusion2D_MPI(const double D, const double L, const int N, const double dt, const int rank, const int procs) : D_(D), L_(L), N_(N), dt_(dt), rank_(rank), procs_(procs)
    {
        /* Real space grid spacing */
        dr_ = L_ / (N_ - 1);

        /* Stencil factor */
        fac_ = dt_ * D_ / (dr_ * dr_);

        /* Number of rows per process */
        local_N_ = N_ / procs_;

        /* Small correction for the last process */
        if (rank_ == procs - 1)
            local_N_ += N_ % procs_;

        /* Actual dimension of a row (+2 for the ghost cells) */
        real_N_ = N_ + 2;

        /* Total number of cells */
        Ntot_ = (local_N_ + 2) * (N_ + 2);

        rho_.resize(Ntot_, 0.0);
        rho_tmp_.resize(Ntot_, 0.0);

        /* Check that the timestep satisfies the restriction for stability */
        if (rank_ == 0) {
            std::cout << "timestep from stability condition is " << dr_ * dr_ / (4. * D_) << '\n';
        }

        initialize_density();
    }


    void advance()
    {

        // TODO: Implement Blocking MPI communication to exchange the ghost
        // cells on a periodic domain required to compute the central finite
        // differences below.

        // *** start MPI part ***
        MPI_Status status[2];

        int prev_rank = rank_ - 1;
        int next_rank = rank_ + 1;

        if (prev_rank < 0) prev_rank = MPI_PROC_NULL;
        if (next_rank >= procs_) next_rank = MPI_PROC_NULL;

        /* Exchange ALL necessary ghost cells with neighboring ranks */
        MPI_Sendrecv(&rho_[1*real_N_+1], N_, MPI_DOUBLE, prev_rank, 0,
            &rho_[(local_N_+1)*real_N_+1], N_, MPI_DOUBLE, next_rank, 0, MPI_COMM_WORLD, &status[0]);

        MPI_Sendrecv(&rho_[local_N_*real_N_+1], N_, MPI_DOUBLE, next_rank, 1,
            &rho_[0*real_N_+1], N_, MPI_DOUBLE, prev_rank, 1, MPI_COMM_WORLD, &status[1]);
        // *** end MPI part ***


        /* Central differences in space, forward Euler in time, Dirichlet BCs */
        for (int i = 1; i <= local_N_; ++i) {
            for (int j = 1; j <= N_; ++j) {
                rho_tmp_[i*real_N_ + j] = rho_[i*real_N_ + j] + fac_ * ( + rho_[i*real_N_ + (j+1)]
                                                                         + rho_[i*real_N_ + (j-1)]
                                                                         + rho_[(i+1)*real_N_ + j]
                                                                         + rho_[(i-1)*real_N_ + j]
                                                                         - 4.*rho_[i*real_N_ + j]
                                                                       );
            }
        }

        /* Use swap instead of rho_ = rho_tmp__. This is much more efficient,
           because it does not copy element by element, just replaces storage
           pointers. */
        using std::swap;
        swap(rho_tmp_, rho_);
    }


    void compute_diagnostics(const double t)
    {
        double heat = 0.0;

        /* Integration to compute total heat */
        for(int i = 1; i <= local_N_; ++i)
            for(int j = 1; j <= N_; ++j)
                heat += rho_[i*real_N_ + j] * dr_ * dr_;

        // TODO: Sum total heat from all ranks

        // *** start MPI part ***
        MPI_Reduce(rank_ == 0? MPI_IN_PLACE: &heat, &heat, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
        // *** end MPI part ***

        if (rank_ == 0) {
            std::cout << "t = " << t << " heat = " << heat << '\n';
            diag.push_back(Diagnostics(t, heat));
        }
    }


    void write_diagnostics(const std::string &filename) const
    {
        std::ofstream out_file(filename, std::ios::out);
        for (const Diagnostics &d : diag)
            out_file << d.time << '\t' << d.heat << '\n';
        out_file.close();
    }


    void compute_histogram_hybrid()
    {
        /* Number of histogram bins */
        const int M = 10;
        int hist[M] = {0};

        /* Find max and min density values */
        double max_rho, min_rho;
        max_rho = rho_[1*real_N_ + 1];
        min_rho = rho_[1*real_N_ + 1];

        for(int i = 1; i <= local_N_; ++i)
            for(int j = 1; j <= N_; ++j) {
                if (rho_[i*real_N_ + j] > max_rho) max_rho = rho_[i*real_N_ + j];
                if (rho_[i*real_N_ + j] < min_rho) min_rho = rho_[i*real_N_ + j];
            }

        // TODO: Compute the global min and max heat values on this rank and
        // store the result in min_rho and max_rho, respectively.
        double lmin_rho = min_rho;
        double lmax_rho = max_rho;

        // *** start MPI part ***
        MPI_Allreduce(&lmin_rho, &min_rho, 1, MPI_DOUBLE, MPI_MIN, MPI_COMM_WORLD);
        MPI_Allreduce(&lmax_rho, &max_rho, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
        // *** end MPI part ***

        double epsilon = 1e-8;
        double dh = (max_rho - min_rho + epsilon) / M;

        for(int i = 1; i <= local_N_; ++i)
            for(int j = 1; j <= N_; ++j) {
                unsigned int bin = (rho_[i*real_N_ + j] - min_rho) / dh;
                hist[bin]++;
            }


        // TODO: Compute the sum of the histogram bins over all ranks and store
        // the result in the array g_hist.  Only rank 0 must print the result.

        // *** start MPI part ***
        int g_hist[M] = {0}; // for the solution
        MPI_Reduce(hist, g_hist, M, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
        // *** end MPI part ***

        if (rank_ == 0)
        {
            printf("=====================================\n");
            printf("Output of compute_histogram_hybrid():\n");
            int gl = 0;
            for (int i = 0; i < M; i++) {
                printf("bin[%d] = %d\n", i, g_hist[i]);
                gl += g_hist[i];
            }
            printf("Total elements = %d\n", gl);
        }

    } //end public


private:

    void initialize_density()
    {
        /* Initialization of the density distribution */
        int gi; // global index
        double bound = 0.25 * L_;

        for (int i = 1; i <= local_N_; ++i) {
            gi = rank_ * (N_ / procs_) + i;	// convert local index to global index
            for (int j = 1; j <= N_; ++j) {
                if (std::abs((gi-1)*dr_ - L_/2.) < bound && std::abs((j-1)*dr_ - L_/2.) < bound) {
                    rho_[i*real_N_ + j] = 1;
                } else {
                    rho_[i*real_N_ + j] = 0;
                }
            }
        }
    }


    double D_, L_;
    int N_, Ntot_, local_N_, real_N_;
    double dr_, dt_, fac_;
    int rank_, procs_;

    std::vector<double> rho_, rho_tmp_;
    std::vector<Diagnostics> diag;
};


int main(int argc, char* argv[])
{
    if (argc < 5) {
        std::cerr << "Usage: " << argv[0] << " D L N dt\n";
        return 1;
    }

    // TODO: Start-up the MPI environment and determine this process' rank ID as
    // well as the total number of processes (=ranks) involved in the
    // communicator

    int rank, procs;

    // *** start MPI part ***
    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &procs);
    // *** end MPI part ***

    const double D = std::stod(argv[1]);
    const double L = std::stod(argv[2]);
    const int N = std::stoul(argv[3]);
    const double dt = std::stod(argv[4]);

    Diffusion2D_MPI system(D, L, N, dt, rank, procs);

    system.compute_diagnostics(0);

    timer t;
    t.start();
    for (int step = 0; step < 10000; ++step) {
        system.advance();
#ifndef _PERF_
        system.compute_diagnostics(dt * step);
#endif
    }
    t.stop();

    if (rank == 0)
        std::cout << "Timing: " << N << ' ' << t.get_timing() << '\n';

    system.compute_histogram_hybrid();

#ifndef _PERF_
    if (rank == 0)
        system.write_diagnostics("diagnostics_mpi.dat");
#endif

    // TODO: Shutdown the MPI environment
    // *** start MPI part ***
    MPI_Finalize();
    // *** end MPI part ***

    return 0;
}
