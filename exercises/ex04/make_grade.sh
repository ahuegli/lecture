#!/usr/bin/env bash
# File       : make_grade.sh
# Description: Generate your exercise grade
# Copyright 2018 ETH Zurich. All Rights Reserved.
#
# EXAMPLE:
# python grade.py \
#     --question1 10 \ # my scored points
#     --comment1 'Add a comment to justify your score' \ # optional
#     --comment1 'Add a comment if you had problems somewhere' \ # optional
#     --question2 50 \ # my scored points
#
# FOR HELP:
# python grade.py --help
#
# The script generates a grade.txt file. Submit your grade on Moodle:
# https://moodle-app2.let.ethz.ch/course/view.php?id=11566

python grade.py \
    --question1 36 \
    --comment1 "correct type, MPI_INIT, MPI_Comm_rank, mpi_finilize, and correct exchange of data"\
    --comment1 "did not use MPI_Reduce in a)" \
    --comment1 "used mpi_allreduce" \
    --comment1 "used mpi_reduce in c) but did not compute histogram" \
    --question2 20 \
    --question3 30
